import react from 'react'
import classes from './MadeBy.module.scss'

class MadeBy extends react.Component {
  state = {
    show: false,
    innerPersons: [
      {
        position: 'Руководитель проекта',
        name: 'Кан Виктор'
      },
      {
        position: 'Веб дизайнер',
        name: 'Аманкелды Акжан'
      },
      {
        position: 'Frontend-разработчик',
        name: 'Ерденбеков Темир'
      },
    ],
    outPersons: [
      {
        position: 'Стилист',
        name: 'Ольга Талипова',
        link: 'https://www.instagram.com/lelya_cvyatok/?hl=ru'
      },
      {
        position: 'Одежда',
        names: [
          {
            name: 'Fashion Retail Group',
            link: 'https://www.instagram.com/frg.kz/?hl=ru'
          },
          {
            name: 'Maria Sarieva',
            link: 'https://www.instagram.com/mariasarievawcl/?hl=ru'
          },
          {
            name: '7am atelier',
            link: 'https://www.instagram.com/7am.atelier'
          },
          {
            name: 'Valet brand',
            link: 'https://www.instagram.com/valet.brand'
          },
          {
            name: 'Dina Tayau',
            link: 'https://www.instagram.com/dina_tayau/?hl=ru'
          },
        ]
      },
    ]
  }

  listShow = () => {
    this.setState({show: !this.state.show})
  }

  render() {
    return (
      <div className={classes.MadeBy}>
        <div className={classes.title} onClick={this.listShow}>
          Над проектом работали
        </div>
        {this.state.show ?
          <div className={classes.list}>
            <div className={classes.innerPersons}>
              <ul>
                {
                  this.state.innerPersons.map((item, i) => {
                    return <li key={i}>
                      <span className={classes.position}>{item.position}: </span>
                      <span className={classes.name}>{item.name}</span>
                    </li>
                  })
                }
              </ul>
            </div>

            <div className={classes.outPersons}>
              <ul>
                {
                  this.state.outPersons.map((item, i) => {
                    return <li key={i}>
                      <span className={classes.position}>{item.position}: </span>
                      {item.name ?
                        <a href={item.link} className={classes.name} target={'_blank'} rel="noreferrer">{item.name}</a>
                        : <span className={classes.names}>
                        {
                          item.names.map((item2, i2) => {
                            return <a href={item2.link} key={i2} className={classes.name} rel="noreferrer"
                                      target={'_blank'}>{item2.name}</a>
                          })
                        }
                      </span>
                      }

                    </li>
                  })
                }
              </ul>
            </div>
          </div>
          : null
        }

      </div>
    )
  }
}

export default MadeBy