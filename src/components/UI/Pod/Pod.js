import react from 'react'
import classes from './Pod.module.scss'

const Pod = props => {
    let link = props.link ? props.link : false

    return (
        <section className={classes.Pod}>
            <div className="container">
                <div className={classes.title}>Материал подготовлен при поддержке</div>
                {
                    link ?
                        <a href="" target={'_blank'} rel="noreferrer"><img
                            src={props.logo}
                            alt="logo"/></a>
                        :
                        <img
                            src={props.logo}
                            alt="logo"/>
                }

            </div>
        </section>
    )
}

export default Pod