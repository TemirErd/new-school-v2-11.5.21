let prefixUrl = ''

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  prefixUrl = '../../..'
}

const images = [
  prefixUrl + '/imgs/logo.png',

  prefixUrl + '/imgs/img1.png',
  prefixUrl + '/imgs/img2.png',

  prefixUrl + '/imgs/img3.png',
  prefixUrl + '/imgs/img4.png',
  prefixUrl + '/imgs/img5.png',

  prefixUrl + '/imgs/img6.png',
  prefixUrl + '/imgs/img7.png',
  prefixUrl + '/imgs/img8.png',
  prefixUrl + '/imgs/img9.png',
  prefixUrl + '/imgs/img10.png',
  prefixUrl + '/imgs/img11.png',
  prefixUrl + '/imgs/img12.png',
  prefixUrl + '/imgs/img13.png',
  prefixUrl + '/imgs/img14.png',
  prefixUrl + '/imgs/img15.png',

  prefixUrl + '/imgs/img16.png',
  prefixUrl + '/imgs/img17.png',
  prefixUrl + '/imgs/img18.png',
  prefixUrl + '/imgs/img19.png',
  prefixUrl + '/imgs/img20.png',
  prefixUrl + '/imgs/img21.png',
  prefixUrl + '/imgs/img22.png',
  prefixUrl + '/imgs/img23.png',

  prefixUrl + '/imgs/img24.png',
  prefixUrl + '/imgs/img25.png',

  prefixUrl + '/imgs/img26.png',

  prefixUrl + '/imgs/img27.png',

  prefixUrl + '/imgs/img28.png',
  prefixUrl + '/imgs/img29.png',
  prefixUrl + '/imgs/img30.png',
  prefixUrl + '/imgs/img31.png',
  prefixUrl + '/imgs/img32.png',
]

export default images