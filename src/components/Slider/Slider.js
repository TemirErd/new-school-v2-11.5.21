import React from 'react'
import classes from './Slider.module.scss'

import {useKeenSlider} from "keen-slider/react"
import "keen-slider/keen-slider.min.css"


const Slider = props => {
  const [currentSlide, setCurrentSlide] = React.useState(0)
  const [pause, setPause] = React.useState(false)
  const timer = React.useRef()

  const isMob = () => {
    if (props.windowSize.width < 760) {
      return true
    }
    return false
  }

  const slidePView = function () {
    if (props.windowSize.width < 760) {
      return 1
    }
    if (props.slidePerView) {
      return props.slidePerView
    }
    return 4
  }

  const spacing = function () {
    if (props.windowSize.width < 960) {
      return 10
    }
    return 30
  }

  const autoPlay = function () {
    if (props.autoPlay === false) {
      return false
    }
    return true
  }

  const centered = function () {
    if (props.centered) {
      return true
    }
    return false
  }

  const arrows = function () {
    if (props.arrows === false) {
      return false
    }
    return true
  }

  const [sliderRef, slider] = useKeenSlider(
    {
      loop: true,
      initial: 0,
      duration: 1000,
      dragStart: () => {
        setPause(true)
      },
      dragEnd: () => {
        setPause(false)
      },
      slideChanged(s) {
        setCurrentSlide(s.details().relativeSlide)
      },
      slidesPerView: slidePView(),
      spacing: spacing(),
      controls: true,
      centered: centered()

    })

  React.useEffect(() => {
    sliderRef.current.addEventListener("mouseover", () => {
      setPause(true)
    })
    sliderRef.current.addEventListener("mouseout", () => {
      setPause(false)
    })
  }, [sliderRef])

  React.useEffect(() => {

    if (autoPlay()) {
      timer.current = setInterval(() => {
        if (!pause && slider) {
          slider.next()
        }
      }, 3000)
      return () => {
        clearInterval(timer.current)
      }
    }

  }, [pause, slider])

  return (
    <>
      <div className="navigation-wrapper">
        <div ref={sliderRef} className={classes.Slider}>
          <div ref={sliderRef} className="keen-slider">
            {props.slides.map((slide, index) => {
              return (
                <div className="keen-slider__slide" key={index}>
                  <div className="imgWrap">
                    <img src={slide.img} alt="slide"/>
                  </div>
                  <div className="name">{slide.name}</div>
                  <div className="txt">{slide.txt}</div>
                  <div className="year">{slide.year}</div>
                </div>
              )
            })}

          </div>
        </div>
        {
          !isMob() && arrows() !== false ?
            slider && (
              <div className="arrows">
                <ArrowLeft
                  onClick={(e) => e.stopPropagation() || slider.prev()}
                  disabled={currentSlide === 0}
                />

                <ArrowRight
                  onClick={(e) => e.stopPropagation() || slider.next()}
                  disabled={currentSlide === slider.details().size - 1}
                />
              </div>
            )
            :
            null
        }

      </div>

      {slider && (
        <div className="dots">
          {[...Array(slider.details().size).keys()].map((idx) => {
            return (
              <button
                key={idx}
                onClick={() => {
                  slider.moveToSlideRelative(idx)
                }}
                className={"dot" + (currentSlide === idx ? " active" : "")}
              />
            )
          })}
        </div>
      )}
    </>
  )
}

function ArrowLeft(props) {
  const disabeld = props.disabled ? " arrow--disabled" : ""
  return (
    <svg
      onClick={props.onClick}
      className={"arrow arrow--left" + disabeld}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
    >
      <path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/>
    </svg>
  )
}

function ArrowRight(props) {
  const disabeld = props.disabled ? " arrow--disabled" : ""
  return (
    <svg
      onClick={props.onClick}
      className={"arrow arrow--right" + disabeld}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
    >
      <path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"/>
    </svg>
  )
}

export default Slider