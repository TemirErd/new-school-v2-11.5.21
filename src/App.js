import React, {Component, Fragment} from 'react'
import Loader from './components/UI/Loader/Loader'
import Slider from './components/Slider/Slider'

import images from './components/Images/Images'

import 'normalize-css'
import './scss/preset.scss'
import './scss/App.scss';
import './scss/media.scss';
//import MadeBy from "./components/UI/MadeBy/MadeBy";
//import Pod from "./components/UI/Pod/Pod";


class App extends Component {
  //DATA=========================================
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      windowState: {
        width: 0,
        height: 0
      },
      currentYear: 0,
      menuOffset: -80,
      //baseUrl: '/'
      baseUrl: '/landing2/'
    }
  }

//METHODS=========================================
  imagesLoading() {
    return images.map((url, idx) => (
      <img src={`${this.state.baseUrl}${url}`} key={idx} alt={'imgLoad'}/>
    ))
  }

  imagesWithBaseUrl(imgId) {
    return `${this.state.baseUrl}${images[imgId]}`
  }

  scrollToElement(e, offset) {
    e.preventDefault()
    const yOffset = offset;
    const element = document.querySelector(e.target.name)
    const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

    window.scrollTo({top: y, behavior: 'smooth'});
  }

  menuClick = (event) => {
    event.preventDefault()
    this.scrollToElement(event, this.state.menuOffset);
    this.menuBurgerHide(event)
  }

  setCurrentYear() {
    let d = new Date();
    let n = d.getFullYear();

    this.setState({currentYear: n})
  }

  menuBurger() {
    const menu = document.querySelector('#navbar .menu')
    menu.classList.toggle('active')
  }

  menuBurgerHide() {
    const menu = document.querySelector('#navbar .menu')
    menu.classList.remove('active')
  }

  onLoadedHandler() {
    this.setState({loading: false}, () => {

    });
  }


  handleTimeout() {
    this.props.showError()
    alert("hit timeout. this is the onTimeout function being run and is mounting normally now")
    this.setState({loading: false})
  }


  handleResize = () => {
    this.setState(prevState => ({
      windowState: {                   // object that we want to update
        ...prevState.windowState,    // keep all other key-value pairs
        width: window.innerWidth,       // update the value of specific key
        height: window.innerHeight,       // update the value of specific key
      }
    }))
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({loading: false})

    }, 500)
    //получить размер экрана
    window.addEventListener('resize', this.handleResize)
    this.handleResize()

    this.setCurrentYear()
    setTimeout(() => {
      if (this.state.windowState.width < 760) {
        this.setState({menuOffset: -50})
      }
    }, 100)


  }

  //VIEW=========================================
  render() {
    return (
      <Fragment>
        {
          this.state.loading
            ? <Loader/>
            : null
        }
        {
          !this.state.loading
            ? <div id="contentInner">
              <section id="navbar">
                <div className="container flex">
                  <img src={this.imagesWithBaseUrl(0)} className="logo" alt="logo"/>

                  {
                    this.state.windowState.width < 760 ?
                      <div className="flex flexMob">
                        <div className="burger" onClick={this.menuBurger}>
                          <div></div>
                          <div></div>
                          <div></div>
                        </div>
                        <a href="tel:87273560007" className=" btn blue top"><span>Позвонить</span></a>
                      </div>
                      : null
                  }

                  <div className={'menu flex'}>
                    <a href="/" onClick={this.menuClick} name="#section1">Обращение директора</a>
                    <a href="/" onClick={this.menuClick} name="#section2">Преимущества</a>
                    <a href="/" onClick={this.menuClick} name="#section3">Наши ученики</a>
                    <a href="/" onClick={this.menuClick} name="#section6">Галерея</a>
                    <a href="/" onClick={this.menuClick} name="#section7">Контакты</a>
                    <a href="mailto:info@newschool.kz" className="mail"><img src={this.imagesWithBaseUrl(26)}
                                                                             alt="img"/><span>info@newschool.kz</span></a>
                    <a href="tel:87273560007" className="call btn blue top"><span>Позвонить</span></a>
                  </div>


                </div>
              </section>
              {/*.navbar*/}

              <section id="top">
                <div className="container">
                  <div className="border"></div>
                  <div className="flex">
                    <div className="item item1">
                      <div className="sub">Больше чем Школа</div>
                      <h1>Новая школа</h1>
                      <div className="desc">
                        Качественное образование — залог счастливого детства
                        и успешного будущего
                      </div>
                      <a href="/" className="btn" name="#section4" onClick={this.menuClick}>Записаться на визит
                      </a>
                    </div>
                    <div className="item item2">
                      <img src={this.imagesWithBaseUrl(1)} alt="img"/>
                    </div>
                  </div>
                </div>
              </section>
              {/*.top*/}
              <section id="section1">
                <div className="container">
                  <div className="flex">
                    <div className="item item1">
                      <img src={this.imagesWithBaseUrl(2)} alt="img"/>
                      <div className="prof">Директор</div>
                      <div className="name">Гульнара Мауткановна Карамендинова</div>
                    </div>
                    <div className="item item2">
                      <div className="title">Дорогие родители!</div>
                      <p>На протяжении 28 лет наша школа обучает творческих и талантливых ребят. Время идет, и мы не
                        стоим на месте. Информационные технологии проникают во все сферы жизни, и наша задача на сегодня
                        — привить нашим детям новые навыки. Новая школа — это место, в котором дети не только получают
                        качественное образование, но и строят крепкие и дружеские отношения.</p>
                      <p>Новая школа — это территория творчества. Мы искренне хотим, чтобы наши дети состоялись, нашли
                        свою нишу в жизни через развитие креативных способностей. В нашей школе мы также учим каждого
                        ребенка не сравнивать себя с другими, а наблюдать исключительно за собой. Ведь так важно
                        отслеживать личный рост, формировать внутреннюю опору и веру в себя.</p>
                      <p>Новая школа - это школа, в которой дети хотят учиться!</p>
                    </div>
                  </div>
                </div>
              </section>
              {/*.section1 обращение директора*/}
              <section id="section2">
                <div className="container">
                  <div className="border"></div>
                  <h2 className="title">Почему родители выбирают нас</h2>
                  <div className="flex">
                    <div className="item">
                      <div className="number">01</div>
                      <div className="txt">Ученики обладатели престижных премий и наград</div>
                    </div>
                    {/*.item*/}
                    <div className="item">
                      <div className="number">02</div>
                      <div className="txt">28 лет безупречной работы</div>
                    </div>
                    {/*.item*/}
                    <div className="item">
                      <div className="number">03</div>
                      <div className="txt">Продвинутые технологии и робототехника</div>
                    </div>
                    {/*.item*/}
                    <div className="item">
                      <div className="number">04</div>
                      <div className="txt">4-х разовое сбалансированное питание</div>
                    </div>
                    {/*.item*/}
                    <div className="item">
                      <div className="number">05</div>
                      <div className="txt">Шахматный клуб, вокал, таеквандо и многое другое</div>
                    </div>
                    {/*.item*/}
                    <div className="item">
                      <div className="number">06</div>
                      <div className="txt">32 международные награды</div>
                    </div>
                    {/*.item*/}
                  </div>
                </div>
              </section>
              {/*.section2 Почему родители выбирают нас*/}
              <section id="section3">
                <div className="container">
                  <div className="border"></div>
                  <div className="titleWrap">
                    <h2 className="title">Наши ученики — победители конкурсов и олимпиад</h2>
                  </div>
                  <div className="sub">Результативность обучения обеспечивает активное включение детей в
                    исследовательскую и проектную деятельность, позволяющую им проявлять ответсвенность и
                    самостоятельность
                  </div>
                  <div className="flex">
                    <div className="item">
                      <img src={this.imagesWithBaseUrl(3)} alt="img"/>
                      <div className="number">15</div>
                      <div className="txt">знаков “Алтын белгі”</div>
                    </div>
                    {/*.item*/}
                    <div className="item">
                      <img src={this.imagesWithBaseUrl(4)} alt="img"/>
                      <div className="number">>50</div>
                      <div className="txt">Призеров международных и республиканских творческих конкурсов
                      </div>
                    </div>
                    {/*.item*/}
                    <div className="item">
                      <img src={this.imagesWithBaseUrl(5)} alt="img"/>
                      <div className="number">>40</div>
                      <div className="txt">Наград в сфере IT технологий</div>
                    </div>
                    {/*.item*/}
                  </div>

                  <div className="slider">
                    <Slider
                      windowSize={this.state.windowState}
                      slides={
                        [
                          {
                            img: this.imagesWithBaseUrl(6),
                            name: 'Ешенкулова Дамель',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2015'
                          },
                          {
                            img: this.imagesWithBaseUrl(7),
                            name: 'Янушко Дарья',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2015'
                          },
                          {
                            img: this.imagesWithBaseUrl(8),
                            name: 'Зайцева Екатерина',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2015'
                          },
                          {
                            img: this.imagesWithBaseUrl(9),
                            name: 'Махамбетова Динара',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2012'
                          },
                          {
                            img: this.imagesWithBaseUrl(10),
                            name: 'Иоффе Элина',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2016'
                          },
                          {
                            img: this.imagesWithBaseUrl(11),
                            name: 'Зинченко Полина',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2019'
                          },
                          {
                            img: this.imagesWithBaseUrl(12),
                            name: 'Зафар Алина',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2019'
                          },
                          {
                            img: this.imagesWithBaseUrl(13),
                            name: 'Филимонова Анна',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2019'
                          },
                          {
                            img: this.imagesWithBaseUrl(14),
                            name: 'Лаврова Арина',
                            txt: 'Награждена знаком «Алтын бельгі»',
                            year: '2016'
                          },
                          {
                            img: this.imagesWithBaseUrl(15),
                            name: 'Банникова Валерия',
                            txt: 'Победитель международного конкурса научных проектов',
                            year: '2016'
                          },
                        ]
                      }
                    />
                  </div>
                </div>
              </section>
              {/*.section3 Наши ученики*/}
              <section id="section4">
                <div className="container">
                  <div className="flex">
                    <div className="item item1">
                      <div className="title">Хотите увидеть все своими глазами?</div>
                      <div className="sub">Прежде чем записать ребенка приглашаем ознакомится со школой ее условиями и
                        атмосферой.
                      </div>
                      <img src={this.imagesWithBaseUrl(27)} className="el" alt="img"/>

                    </div>
                    <div className="item item2">
                      <div id="form">
                        <div className="titleForm">Оставьте свои данные
                          и мы свяжемся с Вами!
                        </div>
                        <form>
                          <input className="input" required placeholder='Имя ребенка' type="text"/>
                          <input className="input" required placeholder='Имя родителя' type="text"/>
                          <input className="input" required placeholder='Телефон' type="text"/>
                          <input type="button" className="btn blue" value={'записаться на визит'}/>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/*.section4.form*/}
              <section id="section5">
                <div className="container">
                  <h3 className="title">Миссия Школы</h3>
                  <div className="flex flex1">
                    <div className="item item1">
                      <div className="flex flex2">
                        <div className="item"><img src={this.imagesWithBaseUrl(28)}
                                                   alt="img"/><span>Личностный рост</span></div>
                        <div className="item"><img src={this.imagesWithBaseUrl(29)}
                                                   alt="img"/><span>Развитие способностей</span>
                        </div>
                        <div className="item"><img src={this.imagesWithBaseUrl(30)} alt="img"/><span>Спокойствие за будущее</span>
                        </div>
                        <div className="item"><img src={this.imagesWithBaseUrl(31)} alt="img"/><span>Реализация компетенций</span>
                        </div>
                        <div className="item"><img src={this.imagesWithBaseUrl(32)} alt="img"/><span>Развитие системы образования</span>
                        </div>
                      </div>
                    </div>
                    <div className="item item2">
                      <p>“Новая школа”</p>
                      <ul>
                        <li>– это современные стандарты обучения на основе традиций классического образования;</li>
                        <li>– это многогранное развитие личности ребёнка;</li>
                        <li>– это здоровьесберегающие условия пребывания ребёнка в школе полного учебного дня;</li>
                        <li>– это сплочённый стабильный коллектив учителей высокой квалификации;</li>
                        <li>– это тесная связь семьи и школы.</li>
                      </ul>
                      <p> “Новая школа” – это устремлённые в будущее учителя, ученики, родители.</p>
                    </div>
                  </div>
                </div>
              </section>
              {/*.section5 Миссия Школы*/}
              <section id="section6">
                <div className="container">
                  <div className="border"></div>
                  <div className="title">Галерея</div>

                  <div className="slider">
                    <div className="bgLeft"></div>
                    <div className="bgRight"></div>
                    <Slider
                      windowSize={this.state.windowState}
                      centered={true}
                      slidePerView={2}
                      autoPlay={true}
                      arrows={false}
                      slides={
                        [
                          {
                            img: this.imagesWithBaseUrl(16),

                          },
                          {
                            img: this.imagesWithBaseUrl(17),

                          },
                          {
                            img: this.imagesWithBaseUrl(18),

                          }, {
                          img: this.imagesWithBaseUrl(19),

                        }, {
                          img: this.imagesWithBaseUrl(20),

                        }, {
                          img: this.imagesWithBaseUrl(21),

                        }, {
                          img: this.imagesWithBaseUrl(22),

                        }, {
                          img: this.imagesWithBaseUrl(23),

                        },
                        ]
                      }
                    />
                  </div>
                </div>
              </section>
              {/*.section6 галерея*/}
              <section id="section7">
                <div className="container">
                  <h3 className="title">Контакты</h3>
                  <div className="border"></div>
                  <div className="flex flex1">
                    <div className="item item1 flex flex2">
                      <a href="tel:87273560007" className="call btn"><span>Позвонить</span></a>

                      <div className="inner">
                        <a href="mailto:info@newschool.kz" className="mail">info@newschool.kz</a>

                        <div className="adres">Республика Казахстан, г.Алматы ул.Утеген батыра 102а</div>

                        <div className="soc flex">
                          <a href="https://www.youtube.com/channel/UC51pTq5T7Ll8ue3ZB4Vx6uw/featured" target="_blank"
                             rel="noreferrer"><img
                            src={this.imagesWithBaseUrl(24)} alt="img"/></a>
                          <a href="https://www.instagram.com/newschool_kz/?hl=ru" target="_blank"
                             rel="noreferrer"><img
                            src={this.imagesWithBaseUrl(25)} alt="img"/></a>
                        </div>
                      </div>

                      <a href="/" className="btn btn1" name="#section4" onClick={this.menuClick}>Записаться на визит
                      </a>
                    </div>
                    <div className="item item2">
                      <div className="map">
                        <iframe
                          title="map"
                          src="https://yandex.ru/map-widget/v1/?um=constructor%3A3a903407cbb7a7b791abe519203755c71824722ee0b989224cfda1151b99c446&amp;source=constructor&amp;scroll=false"
                          width="100%" height="390" frameBorder="0"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/*.section7 контакты*/}
              <section id="footer">
                <div className="container">
                  <div className="flex">
                    <img src={this.imagesWithBaseUrl(0)} alt="img"/>
                    <span>Newschool Copyright © {this.state.currentYear}</span>
                  </div>
                </div>
              </section>
              {/*.footer*/}
              {/* .content*/}
            </div>
            : null
        }
      </Fragment>
    );
  }
}

export default App;
